package info.androidhive.glide.data.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;

@Root(name = "enclosure", strict = false)
public class Enclosure {

    @Attribute
    private String type;

    @Attribute
    private String url;

    @Attribute
    private String length;

    public Enclosure() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "url: " + url;
    }
}
