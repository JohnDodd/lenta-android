package info.androidhive.glide.data.models;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;


@Root(name = "rss", strict = false)
public class RssFeed {
    @Path("channel")
    @ElementList(name = "item", inline = true)
    private List<ArticleDao> articleDaoList;

    public List<ArticleDao> getArticleDaoList() {
        return articleDaoList;
    }

    public void setArticleDaoList(List<ArticleDao> articleDaoList) {
        this.articleDaoList = articleDaoList;
    }

    @Override
    public String toString() {
        return articleDaoList.toString();
    }
}
