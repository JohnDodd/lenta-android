package info.androidhive.glide.data.models;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class ArticleResponse extends RealmObject {

    @Ignore
    public static final int TOP7_ID = 1;
    @Ignore
    public static final int LAST24_ID = 2;
    @Ignore
    public static final int ALL_ID = 3;

    @PrimaryKey
    private int id;

    private RealmList<Article> articleList;

    public ArticleResponse() {
    }

    public ArticleResponse(int id, List<Article> articleList) {
        this.id = id;
        setArticleList(articleList);
    }

    public int getId() {
        return id;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = new RealmList<>();
        this.articleList.addAll(articleList);
    }

    public void setArticleList(RealmList<Article> articleList) {
        this.articleList = articleList;
    }
}
