package info.androidhive.glide.data.repository;

public class RepositoryProvider {

    private static RepositoryProvider instance;
    private final ArticleDataRepository articleRepository;

    public RepositoryProvider() {
        articleRepository = new ArticleDataRepository();
    }

    public static RepositoryProvider getInstance() {
        if (instance == null)
            instance = new RepositoryProvider();
        return instance;
    }

    public ArticleDataRepository getArticleRepository() {
        return articleRepository;
    }
}
