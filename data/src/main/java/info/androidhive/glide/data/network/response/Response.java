package info.androidhive.glide.data.network.response;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Response {
    @Nullable
    private Object answer;

    private RequestResult requestResult;

    public Response() {
        requestResult = RequestResult.ERROR;
    }

    @NonNull
    public RequestResult getRequestResult() {
        return requestResult;
    }

    public Response setRequestResult(@NonNull RequestResult requestResult) {
        this.requestResult = requestResult;
        return this;
    }

    @Nullable
    public <T> T getTypedAnswer() {
        if (answer == null) {
            return null;
        }
        //noinspection unchecked
        return (T) answer;
    }

    @NonNull
    public Response setAnswer(@Nullable Object answer) {
        this.answer = answer;
        return this;
    }

    public void save(@NonNull Context context) {
    }
}
