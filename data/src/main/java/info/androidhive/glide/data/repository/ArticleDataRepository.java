package info.androidhive.glide.data.repository;

import android.util.Log;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidhive.glide.data.cache.ArticlesCacheTransformer;
import info.androidhive.glide.data.models.Article;
import info.androidhive.glide.data.models.ArticleResponse;
import info.androidhive.glide.data.network.ApiFactory;
import info.androidhive.glide.data.network.ApiService;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.realm.Realm;
import io.realm.RealmResults;

import static info.androidhive.glide.data.BuildConfig.TAG;
import static info.androidhive.glide.data.models.ArticleResponse.ALL_ID;
import static info.androidhive.glide.data.models.ArticleResponse.LAST24_ID;
import static info.androidhive.glide.data.models.ArticleResponse.TOP7_ID;

public class ArticleDataRepository {
    private final Map<String, ArticleResponse> map = new HashMap<>();
    private final SparseArray<String> categories = new SparseArray<>();

    ArticleDataRepository() {
        categories.append(TOP7_ID, "Самые важные новости");
        categories.append(LAST24_ID, "Новости за последние сутки");
        categories.append(ALL_ID, "Все категории");

        putList(TOP7_ID, new ArrayList<>());
        putList(LAST24_ID, new ArrayList<>());
        putList(ALL_ID, new ArrayList<>());

        reset().subscribe();
    }

    public Single<ArticleResponse> getArticleResponse(int id) {
        ArticleResponse cache = getResponse(id);
        Single<ArticleResponse> result = Single.just(cache);

        if (cache.getArticleList().isEmpty())
            return reset().andThen(result);

        return result;
    }

    private Single<ArticleResponse> getCache(int id) {
        return Single.just(id).map(cacheHandler);
    }

    public String getCategory(int id) {
        return categories.get(id);
    }

    public Flowable<List<ArticleResponse>> getResponses() {
        Single<List<ArticleResponse>> merge1 = Single.merge(getCache(TOP7_ID), getCache(LAST24_ID), getCache(ALL_ID)).toList();
        Single<List<ArticleResponse>> merge2 = Single.merge(getArticleResponse(TOP7_ID), getArticleResponse(LAST24_ID), getArticleResponse(ALL_ID)).toList();
        return Single.concat(merge1, merge2);
    }

    private Completable reset() {
        ApiService service = ApiFactory.INSTANCE.getMainService();
        return Completable.fromObservable(Single.merge(
                service.getTop7().compose(new ArticlesCacheTransformer(TOP7_ID)),
                service.getLast24().compose(new ArticlesCacheTransformer(LAST24_ID)),
                service.getAll().compose(new ArticlesCacheTransformer(ALL_ID)))
                .doOnNext(this::replace).toObservable());
    }

    private void putList(int id, List<Article> value) {
        map.put(categories.get(id), new ArticleResponse(id, value));
    }

    private ArticleResponse getResponse(int id) {
        return map.get(categories.get(id));
    }

    private void replace(ArticleResponse response) {
        int id = response.getId();
        List<Article> list = getResponse(id).getArticleList();
        list.clear();
        list.addAll(response.getArticleList());
    }

    private final Function<Integer, ArticleResponse> cacheHandler = id -> {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ArticleResponse> all = realm.where(ArticleResponse.class).equalTo("id", id).findAll();
        if (all.isEmpty()) {
            realm.close();
            return new ArticleResponse(id, new ArrayList<>());
        }
        ArticleResponse copy = realm.copyFromRealm(all.first());
        realm.close();
        return copy;
    };

    public Flowable<List<ArticleResponse>> getResponsesReset() {
        return reset().andThen(getResponses());
    }

    public Single<ArticleResponse> getArticleResponseReset(int id) {
        return reset().andThen(getArticleResponse(id));
    }
}
