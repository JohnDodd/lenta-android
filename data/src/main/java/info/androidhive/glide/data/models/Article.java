package info.androidhive.glide.data.models;

import org.simpleframework.xml.Element;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Article extends RealmObject {

    @PrimaryKey
    private String guid;

    private String title;

    private String pubDate;

    private String link;

    private String category;

    private String description;

    private String enclosureUrl;

    public Article() {
    }

    public Article(ArticleDao dao) {
        guid = dao.getGuid();
        title = dao.getTitle();
        pubDate = dao.getPubDate();
        link = dao.getLink();
        category = dao.getCategory();
        description = dao.getDescription();
        enclosureUrl = dao.getEnclosureUrl();
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnclosureUrl() {
        return enclosureUrl;
    }

    public void setEnclosureUrl(String enclosureUrl) {
        this.enclosureUrl = enclosureUrl;
    }
}
