package info.androidhive.glide.data.network

import info.androidhive.glide.data.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.util.concurrent.TimeUnit

object ApiFactory {
    private val TIMEOUT = 10
    private val WRITE_TIMEOUT = 10
    private val CONNECT_TIMEOUT = 10

    val mainService: ApiService by lazy { buildRetrofit().create(ApiService::class.java) }

    val client: OkHttpClient by lazy { buildClient() }

    private fun buildRetrofit(): Retrofit
            = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    private fun buildClient()
            = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor())
            .build()
}
