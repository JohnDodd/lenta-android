package info.androidhive.glide.data.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@Root(name = "item", strict = false)
public class ArticleDao {

    @Element(name = "guid", required = false)
    private String guid;

    @Element(name = "title", required = false)
    private String title;

    @Element(name = "pubDate", required = false)
    private String pubDate;

    @Element(name = "link", required = false)
    private String link;

    @Element(name = "category", required = false)
    private String category;

    @Element(name = "description", required = false)
    private String description;

    @Element(name = "enclosure", required = false)
    private Enclosure enclosure;

    public ArticleDao() {
    }

    public ArticleDao(String guid, String title, String pubDate, String link, String category, String description) {
        this.guid = guid;
        this.title = title;
        this.pubDate = pubDate;
        this.link = link;
        this.category = category;
        this.description = description;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    @Override
    public String toString() {
        return "title: " + title + ", description: " + description + ", enclosure: " + enclosure + "\n";
    }

    public String getEnclosureUrl() {
        return enclosure == null ? "" : enclosure.getUrl();
    }
}
