package info.androidhive.glide.data.cache;

import android.util.Log;

import java.util.ArrayList;

import info.androidhive.glide.data.models.Article;
import info.androidhive.glide.data.models.ArticleResponse;
import info.androidhive.glide.data.models.RssFeed;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;
import io.reactivex.functions.Function;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import static info.androidhive.glide.data.BuildConfig.TAG;

public class ArticlesCacheTransformer implements SingleTransformer<RssFeed, ArticleResponse> {

    private int id;
    private Realm realm;

    public ArticlesCacheTransformer(int id) {
        this.id = id;
    }

    private final Function<Throwable, Single<ArticleResponse>> cacheErrorHandler = throwable -> {
        realm = Realm.getDefaultInstance();
        RealmResults<ArticleResponse> all = realm.where(ArticleResponse.class).equalTo("id", id).findAll();
        if (all.isEmpty()) {
            realm.close();
            return Single.just(new ArticleResponse(id, new ArrayList<>()));
        }
        Single<ArticleResponse> copy = Single.just(realm.copyFromRealm(all.first()));
        realm.close();
        return copy;
    };

    @Override
    public SingleSource<ArticleResponse> apply(Single<RssFeed> articles) {
        return articles
                .map(RssFeed::getArticleDaoList)
                .toObservable()
                .flatMapIterable(articleDaos -> articleDaos)
                .map(Article::new)
                .toList()
                .map(articles1 -> new ArticleResponse(id, articles1))
                .doOnSuccess(this::save)
                .onErrorResumeNext(cacheErrorHandler);
    }

    private void save(ArticleResponse articles) {
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(articles);
        realm.commitTransaction();
        realm.close();
    }
}
