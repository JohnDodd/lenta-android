package info.androidhive.glide.data.network;

import info.androidhive.glide.data.models.RssFeed;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("top7")
    Single<RssFeed> getTop7();

    @GET("last24")
    Single<RssFeed> getLast24();

    @GET("articles")
    Single<RssFeed> getAll();
}
