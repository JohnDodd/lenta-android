package tomsksoft.lenta.app

import android.app.Application
import android.content.Context

import com.facebook.stetho.Stetho
import com.uphyca.stetho_realm.RealmInspectorModulesProvider

import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.rx.RealmObservableFactory

class LentaApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        //Realm.removeDefaultConfiguration();
        val configuration = RealmConfiguration.Builder()
                .rxFactory(RealmObservableFactory())
                .build()
        Realm.setDefaultConfiguration(configuration)

        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                .build())
    }

    companion object {
        val appContext = this
    }
}
