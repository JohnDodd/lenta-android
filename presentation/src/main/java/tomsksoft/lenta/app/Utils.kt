package tomsksoft.lenta.app

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import tomsksoft.lenta.R
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun String.parseDate(): String {
    val format = DecimalFormat("00")
    val pattern = "EEE, dd MMM yyyy HH:mm:ss z"
    val date: Date
    try {
        date = SimpleDateFormat(pattern, Locale.ENGLISH).parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
        return this
    }

    val calendar = GregorianCalendar.getInstance()
    calendar.time = date

    return format.format(calendar.get(Calendar.HOUR_OF_DAY).toLong()) + ":" + format.format(calendar.get(Calendar.MINUTE).toLong())

}

fun <T : ImageView> T.loadImage(imageUrl: String) {
    Glide.with(context)
            .load(imageUrl)
            .placeholder(R.color.colorDivider)
            .fitCenter()
            .crossFade()
            .thumbnail(0.5f)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(this)
}

