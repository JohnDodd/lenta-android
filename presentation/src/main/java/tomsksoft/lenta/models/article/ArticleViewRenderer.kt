package tomsksoft.lenta.models.article

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import tomsksoft.lenta.R
import tomsksoft.lenta.app.loadImage
import tomsksoft.lenta.app.parseDate
import tomsksoft.lenta.models.article.ArticleModel.Companion.TYPE
import tomsksoft.lenta.models.article.ArticleViewRenderer.ArticleViewHolder

class ArticleViewRenderer(context: Context, private val listener: Listener)
    : ViewRenderer<ArticleModel, ArticleViewHolder>(TYPE, context) {

    override fun bindView(model: ArticleModel, holder: ArticleViewHolder) {
        with(holder) {
            title.text = model.title
            timeStamp.text = model.timeStamp.parseDate()
            imageView.visibility = if (model.hasImage) VISIBLE else GONE
            imageView.loadImage(model.imageUrl ?: "")
            parent.setOnClickListener { listener.onClick(model) }
        }
    }

    override fun createViewHolder(parent: ViewGroup?): ArticleViewHolder {
        return ArticleViewHolder(inflate(R.layout.item_article_line, parent))
    }

    interface Listener {
        fun onClick(model: ArticleModel)
    }

    class ArticleViewHolder constructor(itemView: View) : ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.image)
        val timeStamp: TextView = itemView.findViewById(R.id.timeStamp)
        val title: TextView = itemView.findViewById(R.id.title)
        val parent: ConstraintLayout = itemView.findViewById(R.id.constraint_layout)
    }
}
