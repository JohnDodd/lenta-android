package tomsksoft.lenta.models.article

import info.androidhive.glide.data.models.Article
import tomsksoft.lenta.models.BaseModel

class ArticleModel(id: Int, val title: String, val timeStamp: String, val link: String, val imageUrl: String?, val hasImage: Boolean) : BaseModel(id) {

    constructor(id: Int, article: Article)
            : this(id, article.title, article.pubDate, article.link, article.enclosureUrl, !article.enclosureUrl.isEmpty())

    override fun getType() = TYPE

    override fun toString() = title

    companion object {
        const val TYPE = 1
    }
}
