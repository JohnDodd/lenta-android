package tomsksoft.lenta.models

import com.github.vivchar.rendererrecyclerviewadapter.ItemModel

abstract class BaseModel(val id: Int) : ItemModel {
    companion object {
        val TOP7_CATEGORY_ID = 0
        val TOP7_LIST_ID = 1
        val LAST24_CATEGORY_ID = 2
        val LAST24_LIST_ID = 3
        val ALL_CATEGORY_ID = 4
        val ALL_LIST_ID = 5
    }
}
