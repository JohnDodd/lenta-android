package tomsksoft.lenta.models.category

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import tomsksoft.lenta.R
import tomsksoft.lenta.models.category.CategoryModel.Companion.TYPE
import tomsksoft.lenta.models.category.CategoryViewRenderer.CategoryViewHolder

class CategoryViewRenderer(context: Context, private val listener: Listener)
    : ViewRenderer<CategoryModel, CategoryViewHolder>(TYPE, context) {

    override fun bindView(model: CategoryModel, holder: CategoryViewHolder) {
        holder.title.text = model.title
        holder.parent.setOnClickListener { listener.onClick(model) }
    }

    override fun createViewHolder(parent: ViewGroup?): CategoryViewHolder {
        return CategoryViewHolder(inflate(R.layout.item_article_title, parent))
    }

    interface Listener {
        fun onClick(model: CategoryModel)
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.title)
        val parent: ConstraintLayout = itemView.findViewById(R.id.constraint_layout)
    }
}
