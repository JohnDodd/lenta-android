package tomsksoft.lenta.models.category

import tomsksoft.lenta.models.BaseModel

class CategoryModel(id: Int, val title: String) : BaseModel(id) {

    override fun getType() = TYPE

    override fun toString() = title

    companion object {
        const val TYPE = 0
    }
}
