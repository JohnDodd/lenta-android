package tomsksoft.lenta.models.square

import info.androidhive.glide.data.models.Article
import tomsksoft.lenta.models.BaseModel

class SquareModel(id: Int, val title: String, val link: String, val imageUrl: String?) : BaseModel(id) {

    constructor(id: Int, article: Article) : this(id, article.title, article.link, article.enclosureUrl)

    override fun getType() = TYPE

    companion object {
        val TYPE = 2
    }

}
