package tomsksoft.lenta.models.square

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import tomsksoft.lenta.R
import tomsksoft.lenta.app.loadImage
import tomsksoft.lenta.models.square.SquareModel.Companion.TYPE
import tomsksoft.lenta.models.square.SquareViewRenderer.SquareViewHolder

class SquareViewRenderer(context: Context, private val listener: Listener)
    : ViewRenderer<SquareModel, SquareViewHolder>(TYPE, context) {

    override fun bindView(model: SquareModel, holder: SquareViewHolder) {
        holder.title.text = model.title
        holder.imageView.loadImage(model.imageUrl ?: "")
        holder.parent.setOnClickListener { listener.onClick(model) }
    }

    override fun createViewHolder(parent: ViewGroup?)
            = SquareViewHolder(inflate(R.layout.item_article_square, parent))


    interface Listener {
        fun onClick(model: SquareModel)
    }

    class SquareViewHolder(itemView: View) : ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.image)
        val title: TextView = itemView.findViewById(R.id.title)
        val parent: ConstraintLayout = itemView.findViewById(R.id.constraint_layout)
    }
}
