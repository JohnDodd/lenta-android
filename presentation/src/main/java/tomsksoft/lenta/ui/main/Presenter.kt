package tomsksoft.lenta.ui.main

import info.androidhive.glide.data.network.ApiFactory
import okhttp3.OkHttpClient

class Presenter(private val view: IView) {
    private val client: OkHttpClient = ApiFactory.client
}
