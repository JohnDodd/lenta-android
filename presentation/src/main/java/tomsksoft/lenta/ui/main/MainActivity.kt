package tomsksoft.lenta.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import tomsksoft.lenta.R
import tomsksoft.lenta.ui.categories.CategoriesFragment
import tomsksoft.lenta.ui.categories.CategoriesFragment.OnFragmentInteractionListener
import tomsksoft.lenta.ui.group.GroupFragment

class MainActivity : AppCompatActivity(), OnFragmentInteractionListener, IView {
    private val presenter by lazy { Presenter(this@MainActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        var fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment == null) {
            fragment = CategoriesFragment()
            supportFragmentManager.beginTransaction().replace(R.id.container, fragment).commit()
        }
    }

    override fun startGroupFragment(categoryId: Int) {
        val fragment = GroupFragment.newInstance(categoryId)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.addToBackStack(null)
        transaction.replace(R.id.container, fragment).commit()
    }
}

interface IView
