package tomsksoft.lenta.ui.categories;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.glide.data.models.Article;
import tomsksoft.lenta.R;

import static android.view.View.GONE;
import static tomsksoft.lenta.app.Utils.parseDate;
import static tomsksoft.lenta.ui.categories.LentaItem.LIST;
import static tomsksoft.lenta.ui.categories.LentaItem.TITLE;

class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<LentaItem> items;
    private Context context;

    Adapter(Context context) {
        this.context = context;
        items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TITLE)
            return new ViewHolderTitle(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article_title, parent, false));
        if (viewType == LIST)
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article_line, parent, false));
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);

        switch (viewType) {
            case TITLE:
                ViewHolderTitle holderTitle = (ViewHolderTitle) viewHolder;
                holderTitle.title.setText(R.string.top_7_category);
                break;
            case LIST:
                ViewHolder holder = (ViewHolder) viewHolder;
                fill(holder, items.get(position).getArticle());
                break;
        }

        /*switch (viewType) {
            case TOP7_TITLE:
                holderTitle = (ViewHolderTitle) viewHolder;
                if (articlesTop7.isEmpty())
                    ((View) holderTitle.title.getParent()).setVisibility(GONE);
                else {
                    ((View) holderTitle.title.getParent()).setVisibility(View.VISIBLE);
                    holderTitle.title.setText(R.string.top_7_category);
                }
                break;
            case TOP7_LIST:
                holder = (ViewHolder) viewHolder;
                Article article = articlesTop7.get(position);
                fill(holder, article);
                break;
            case LAST24_TITLE:
                holderTitle = (ViewHolderTitle) viewHolder;
                if (articlesLast24.isEmpty())
                    ((View) holderTitle.title.getParent()).setVisibility(GONE);
                else {
                    ((View) holderTitle.title.getParent()).setVisibility(View.VISIBLE);
                    holderTitle.title.setText(R.string.last_24_category);
                }
                break;
            case LAST24_LIST:
                holder = (ViewHolder) viewHolder;
                article = articlesLast24.get(position);
                fill(holder, article);
                break;
            case ALL_TITLE:
                holderTitle = (ViewHolderTitle) viewHolder;

                if (articlesLast24.isEmpty())
                    ((View) holderTitle.title.getParent()).setVisibility(GONE);
                else {
                    ((View) holderTitle.title.getParent()).setVisibility(View.VISIBLE);
                    holderTitle.title.setText(R.string.all_categories);
                }
                break;
            case ALL_LIST:
                holder = (ViewHolder) viewHolder;
                article = allArticles.get(position);
                fill(holder, article);
                break;
        }*/
    }

    private void fill(ViewHolder holder, Article article) {
        ImageView imageView = holder.imageView;
        holder.title.setText(article.getTitle());
        holder.time.setText(parseDate(article.getPubDate()));
        String enclosureUrl = article.getEnclosureUrl();
        if (enclosureUrl.equalsIgnoreCase("null")) {
            ((View) imageView.getParent()).setVisibility(GONE);
        } else {
            Glide.with(context)
                    .load(enclosureUrl)
                    .placeholder(R.color.colorDivider)
                    .fitCenter()
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateItems(List<LentaItem> items){
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView time;
        TextView title;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            time = (TextView) itemView.findViewById(R.id.timeStamp);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    public class SquareHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView time;
        TextView title;

        SquareHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            time = (TextView) itemView.findViewById(R.id.timeStamp);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    public class ViewHolderTitle extends RecyclerView.ViewHolder {
        TextView title;

        ViewHolderTitle(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}
