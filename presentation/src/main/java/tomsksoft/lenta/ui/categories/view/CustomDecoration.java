package tomsksoft.lenta.ui.categories.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import tomsksoft.lenta.models.category.CategoryModel;
import tomsksoft.lenta.models.square.SquareModel;

public class CustomDecoration extends RecyclerView.ItemDecoration {
    private final int[] ATTR = new int[]{android.R.attr.listDivider};
    private Drawable divider;
    private int offset;

    public CustomDecoration(Context context) {
        final TypedArray array = context.obtainStyledAttributes(ATTR);
        divider = array.getDrawable(0);
        offset = 16;
        array.recycle();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
        //drawVertical(c, parent);
    }

    public void drawVertical(Canvas canvas, RecyclerView parent) {
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + offset;
            divider.setBounds(left, top, right, bottom);
            divider.draw(canvas);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);
        int viewType = parent.getAdapter().getItemViewType(position);
        switch (viewType) {
            case SquareModel.Companion.getTYPE():
                outRect.set(offset / 2, 0, offset / 2, offset);
                break;
            case CategoryModel.Companion.getTYPE():
                outRect.set(0, 0, 0, offset);
                break;
            default:
                outRect.set(0, 0, 0, offset*2);
                break;
        }

    }
}
