package tomsksoft.lenta.ui.categories;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import info.androidhive.glide.data.models.Article;
import info.androidhive.glide.data.models.ArticleResponse;
import info.androidhive.glide.data.repository.ArticleDataRepository;
import info.androidhive.glide.data.repository.RepositoryProvider;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import tomsksoft.lenta.models.BaseModel;
import tomsksoft.lenta.models.article.ArticleModel;
import tomsksoft.lenta.models.category.CategoryModel;
import tomsksoft.lenta.models.square.SquareModel;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static info.androidhive.glide.data.models.ArticleResponse.TOP7_ID;


class Presenter {
    private static final String TAG = "Presenter";
    private IView view;
    private CompositeDisposable compositeDisposable;

    private ArticleDataRepository repository;
    private List<BaseModel> items;

    Presenter(IView view) {
        this.view = view;
        compositeDisposable = new CompositeDisposable();
        repository = RepositoryProvider.getInstance().getArticleRepository();
        items = new ArrayList<>();
    }

    void onCreateView() {
        Log.d(TAG, "presenter.onCreateView: ");
        compositeDisposable.add(repository.getResponses()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(throwable -> view.showError(throwable.getLocalizedMessage()))
                .doOnSubscribe(disposable -> view.showLoadingIndicator())
                .doAfterTerminate(view::hideLoadingIndicator)
                .doOnComplete(this::checkIsEmpty)
                .subscribe(this::addToList));
    }

    void onRefresh() {
        Log.d(TAG, "onRefresh");
        compositeDisposable.add(repository.getResponsesReset()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(throwable -> view.showError(throwable.getLocalizedMessage()))
                .doOnSubscribe(disposable -> view.showLoadingIndicator())
                .doAfterTerminate(view::hideLoadingIndicator)
                .doOnComplete(this::checkIsEmpty)
                .subscribe(this::addToList));
    }

    private void addToList(List<ArticleResponse> responses) {
        items.clear();
        for (ArticleResponse response : responses) {
            addToList(response);
        }
        update();
    }

    private void addToList(List<Article> articles, int id) {
        if (!articles.isEmpty())
            items.add(new CategoryModel(convertIdToCategoryPosition(id), repository.getCategory(id)));

        for (int i = 0; i < articles.size(); i++) {
            if (i >= 4) return;
            Article article = articles.get(i);
            if (id == TOP7_ID)
                items.add(new ArticleModel(convertIdToListPosition(id), article));
            else
                items.add(new SquareModel(convertIdToListPosition(id), article));

        }
        Collections.sort(items, (o1, o2) -> o1.getId() - o2.getId());
    }

    public void onCategorySelected(CategoryModel model) {
        int id = model.getId();
        id = convertPositionToCategoryId(id);
        view.startGroupFragment(id);
    }

    private void addToList(ArticleResponse response) {
        addToList(response.getArticleList(), response.getId());
    }

    private void update() {
        view.updateAdapter(items);
    }

    private void checkIsEmpty() {
        view.setEmptyViewVisibility(items.isEmpty() ? VISIBLE : INVISIBLE);
    }

    void onStop() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }

    private int convertIdToListPosition(int id) {
        return id + id - 1;
    }

    private int convertIdToCategoryPosition(int id) {
        return (id - 1) * 2;
    }

    private int convertPositionToCategoryId(int id) {
        return id / 2 + 1;
    }
}
