package tomsksoft.lenta.ui.categories;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;

import java.util.List;

import tomsksoft.lenta.R;
import tomsksoft.lenta.models.BaseModel;
import tomsksoft.lenta.models.article.ArticleViewRenderer;
import tomsksoft.lenta.models.category.CategoryViewRenderer;
import tomsksoft.lenta.models.square.SquareViewRenderer;
import tomsksoft.lenta.ui.categories.view.CustomDecoration;

public class CategoriesFragment extends Fragment implements IView {
    private static final String TAG = "CategoriesFragment";

    private OnFragmentInteractionListener listener;
    private Presenter presenter;

    private RecyclerView recyclerView;
    private RendererRecyclerViewAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView emptyErrorView;
    private View v;

    public CategoriesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new Presenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_categories, container, false);
        Log.d(TAG, "onCreateView: ");
        findViews();
        setupRecycler();
        presenter.onCreateView();
        return v;
    }

    private void findViews() {
        emptyErrorView = (TextView) v.findViewById(R.id.empty_error_tv);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(presenter::onRefresh);
    }

    private void setupRecycler() {
        adapter = new RendererRecyclerViewAdapter();
        adapter.registerRenderer(new CategoryViewRenderer(getContext(), presenter::onCategorySelected));
        adapter.registerRenderer(new ArticleViewRenderer(getContext(), model -> startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(model.getLink())))));
        adapter.registerRenderer(new SquareViewRenderer(getContext(), model -> startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(model.getLink())))));

        GridLayoutManager manager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                BaseModel item = adapter.getItem(position);
                if (item.getId() == BaseModel.Companion.getLAST24_LIST_ID() || item.getId() == BaseModel.Companion.getALL_LIST_ID())
                    return 1;
                else return 2;
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new CustomDecoration(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    @Override
    public void showError(String message) {
        Log.d(TAG, "showError: " + message);
    }

    @Override
    public void showLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void updateAdapter(List<BaseModel> items) {
        Log.d(TAG, "updateAdapter: " + items.size());
        Log.d(TAG, "" + items);
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setEmptyViewVisibility(int visibility) {
        emptyErrorView.setVisibility(visibility);
    }

    @Override
    public void startGroupFragment(int categoryId) {
        listener.startGroupFragment(categoryId);
    }

    public interface OnFragmentInteractionListener {
        void startGroupFragment(int categoryId);
    }
}
