package tomsksoft.lenta.ui.categories;

import info.androidhive.glide.data.models.Article;

class LentaItem {
    public static final int TITLE = 1;
    public static final int LIST = 2;

    private int type;
    private Article article;
    private String title;

    public LentaItem(int type, Article article) {
        this.type = type;
        this.article = article;
    }

    public LentaItem(int type, String title) {
        this.type = type;
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public Article getArticle() {
        return article;
    }
}
