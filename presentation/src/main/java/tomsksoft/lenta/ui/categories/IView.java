package tomsksoft.lenta.ui.categories;

import java.util.List;

import tomsksoft.lenta.models.BaseModel;

public interface IView {
    void showError(String message);

    void showLoadingIndicator();

    void hideLoadingIndicator();

    String getString(int id);

    void updateAdapter(List<BaseModel> items);

    void setEmptyViewVisibility(int visibility);

    void startGroupFragment(int categoryId);
}
