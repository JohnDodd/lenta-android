package tomsksoft.lenta.ui.group;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.glide.data.models.ArticleResponse;
import info.androidhive.glide.data.repository.ArticleDataRepository;
import info.androidhive.glide.data.repository.RepositoryProvider;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import tomsksoft.lenta.models.article.ArticleModel;

import static tomsksoft.lenta.BuildConfig.TAG;
import static tomsksoft.lenta.models.BaseModel.ALL_LIST_ID;

class Presenter {
    private IView view;
    private CompositeDisposable compositeDisposable;
    private ArticleDataRepository repository;
    private List<ArticleModel> items;

    Presenter(IView view) {
        this.view = view;
        compositeDisposable = new CompositeDisposable();
        repository = RepositoryProvider.getInstance().getArticleRepository();
        items = new ArrayList<>();
    }

    void onCreateView() {
        Log.d(TAG, "presenter.onCreateView: ");
        compositeDisposable.add(repository.getArticleResponse(view.getCategoryId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(throwable -> Log.d(TAG, "onRefresh: " + throwable.getLocalizedMessage()))
                .doOnSubscribe(disposable -> view.showLoadingIndicator())
                .doAfterTerminate(view::hideLoadingIndicator)
                .subscribe(this::addToList));
    }


    void onRefresh() {
        Log.d(TAG, "onRefresh: " + view.getCategoryId());
        compositeDisposable.add(repository.getArticleResponseReset(view.getCategoryId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnError(throwable -> Log.d(TAG, "onRefresh: " + throwable.getLocalizedMessage()))
                .doOnSubscribe(disposable -> view.showLoadingIndicator())
                .doAfterTerminate(view::hideLoadingIndicator)
                .subscribe(this::addToList));
    }

    private void addToList(ArticleResponse response) {
        items.clear();
        Log.d(TAG, "addToList: " + response.getArticleList().size());
        Observable.fromIterable(response.getArticleList()).map(article ->
                new ArticleModel(Companion.getALL_LIST_ID(), article))
                .take(15)
                .doOnSubscribe(disposable -> items.clear())
                .doOnNext(items::add)
                .toList()
                .subscribe(articleModels -> {
                    items.clear();
                    view.update(articleModels);
                });
    }


    void onStop() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }
}
