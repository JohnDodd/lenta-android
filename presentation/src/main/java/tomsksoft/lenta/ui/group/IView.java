package tomsksoft.lenta.ui.group;

import java.util.List;

import tomsksoft.lenta.models.article.ArticleModel;

interface IView {
    void showLoadingIndicator();

    void hideLoadingIndicator();

    void update(List<ArticleModel> items);

    int getCategoryId();
}
