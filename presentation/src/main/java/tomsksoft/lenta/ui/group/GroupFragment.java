package tomsksoft.lenta.ui.group;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;

import java.util.List;

import tomsksoft.lenta.R;
import tomsksoft.lenta.models.article.ArticleModel;
import tomsksoft.lenta.models.article.ArticleViewRenderer;
import tomsksoft.lenta.ui.categories.view.CustomDecoration;

public class GroupFragment extends Fragment implements IView {
    private static final String CATEGORY_NAME = "CATEGORY_NAME";

    private int categoryId;
    private Presenter presenter;

    private RecyclerView recyclerView;
    private RendererRecyclerViewAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView emptyErrorView;
    private View v;

    public GroupFragment() {
    }

    public static GroupFragment newInstance(int categoryId) {
        GroupFragment fragment = new GroupFragment();
        Bundle args = new Bundle();
        args.putInt(CATEGORY_NAME, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getInt(CATEGORY_NAME);
        }
        presenter = new Presenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_group, container, false);
        findViews();
        setupRecycler();
        presenter.onCreateView();
        return v;
    }

    private void setupRecycler() {
        adapter = new RendererRecyclerViewAdapter();
        adapter.registerRenderer(new ArticleViewRenderer(getContext(), model -> startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(model.getLink())))));
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new CustomDecoration(getContext()));
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void findViews() {
        emptyErrorView = (TextView) v.findViewById(R.id.empty_error_tv);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(presenter::onRefresh);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public void showLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void update(List<ArticleModel> items) {
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getCategoryId() {
        return categoryId;
    }
}
